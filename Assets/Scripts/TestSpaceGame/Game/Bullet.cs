using UnityEngine;
using Zenject;

namespace TestSpaceGame
{
    public class Bullet : Unit
    {
        [Inject]
        protected override void Constructor(int life, int demage, float speed)
        {
            base.Constructor(life, demage, speed);
            movable.Direction = new Vector3(1, 0, 0);
        }

        protected override void DestroySelf()
        {
            Destroy(gameObject);
        }

        public class Factory : PlaceholderFactory<int, int, float, Bullet> { }
    }
}
