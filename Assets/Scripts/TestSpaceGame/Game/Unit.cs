using UnityEngine;

namespace TestSpaceGame
{
    [RequireComponent(typeof(Movable))]
    public abstract class Unit : MonoBehaviour, IUnit, IMovable
    {
        public virtual int Life { get; set; } = 1;
        public virtual int Demage { get; set; } = 1;

        public float Speed { get => movable.Speed; set => movable.Speed = value; }
        public Vector3 Direction { get => movable.Direction; set => movable.Direction = value; }
        public Vector3 Position { get => movable.Position; set => movable.Position = value; }

        protected Movable movable;

        protected virtual void Constructor(int life, int demage, float speed)
        {
            movable = GetComponent<Movable>();

            Life = life;
            Demage = demage;
            Speed = speed;
        }

        public virtual void ApplyCollision(IUnit unit)
        {
            Life -= unit.Demage;
            if (Life <= 0) DestroySelf();
        }

        protected abstract void DestroySelf();
    }
}
