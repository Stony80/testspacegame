using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace TestSpaceGame
{
    public class Enemy : Unit
    {
        [SerializeField]
        private Sprite[] _sprites;

        private SpriteRenderer _spriteRenderer;

        [Inject]
        protected override void Constructor(int life, int demage, float speed)
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();

            base.Constructor(life, demage, speed);
            movable.Direction = new Vector3(-1, 0, 0);
        }

        public override int Life
        {
            set
            {
                base.Life = value;
                if (value > 0)
                {
                    _spriteRenderer.sprite = _sprites[value - 1];

                    PolygonCollider2D polygonCollider = GetComponent<PolygonCollider2D>();
                    polygonCollider.pathCount = _spriteRenderer.sprite.GetPhysicsShapeCount();

                    List<Vector2> path = new List<Vector2>();
                    for (int i = 0; i < polygonCollider.pathCount; i++)
                    {
                        path.Clear();
                        _spriteRenderer.sprite.GetPhysicsShape(i, path);
                        polygonCollider.SetPath(i, path.ToArray());
                    }
                }
            }
            get => base.Life;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            IUnit unit = collision.GetComponent<IUnit>();
            unit.ApplyCollision(this);
            ApplyCollision(unit);
        }

        private void OnDisable()
        {
            DestroySelf();
        }

        protected override void DestroySelf()
        {
            Destroy(gameObject);
        }

        public class Factory : PlaceholderFactory<int, int, float, Enemy> { }
    }
}