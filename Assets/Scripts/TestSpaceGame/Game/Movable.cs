using UnityEngine;

namespace TestSpaceGame
{
    public class Movable : MonoBehaviour, IMovable
    {
        public virtual float Speed { get; set; }
        public virtual Vector3 Direction { get; set; }
        public virtual Vector3 Position { get => transform.position; set => transform.position = value; }

        private void Update()
        {
            Position = transform.position + Direction * Time.deltaTime * Speed;
        }
    }
}