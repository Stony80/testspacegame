using UnityEngine;

namespace TestSpaceGame
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField]
        private Ship _ship;

        private void Update()
        {
            Vector3 direction = Vector3.zero;

            if (Input.GetKey(KeyCode.W))
            {
                direction += Vector3.up;
            }

            if (Input.GetKey(KeyCode.S))
            {
                direction += Vector3.down;
            }

            if (Input.GetKey(KeyCode.A))
            {
                direction += Vector3.left;
            }

            if (Input.GetKey(KeyCode.D))
            {
                direction += Vector3.right;
            }

            _ship.Direction = direction;

            if (Input.GetKey(KeyCode.Space))
                _ship.Shot();

        }
    }
}