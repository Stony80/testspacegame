namespace TestSpaceGame
{
    public class MoveUnit : Movable
    {
        private void OnBecameInvisible()
        {
            Destroy(gameObject);
        }
    }
}
