namespace TestSpaceGame
{
    public interface IUnit
    {
        int Life
        {
            get; set;
        }
        int Demage
        {
            get; set;
        }
        void ApplyCollision(IUnit unit);
    }
}
