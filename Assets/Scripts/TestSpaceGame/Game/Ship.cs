using UnityEngine;
using Zenject;

namespace TestSpaceGame
{
    public class Ship : Unit
    {
        [Inject]
        private Bullet.Factory _bulletFactory;

        [SerializeField]
        private float _shotDelaySeconds;
        private float _nextShotTime;

        [SerializeField]
        private int _life;
        [SerializeField]
        private float _speed;

        private readonly Vector3 _startPosition = new Vector3(-7, 0, 0);

        private void Start()
        {
            Constructor(_life, 1, _speed);
        }

        public void Restart()
        {
            Life = _life;
            gameObject.SetActive(true);
            transform.position = _startPosition;
        }

        public void Shot()
        {
            if (_nextShotTime <= Time.time)
            {
                Bullet bullet = _bulletFactory.Create(1, 1, 6);
                bullet.transform.position = transform.position;
                bullet.transform.parent = transform.parent;
                _nextShotTime = Time.time+ _shotDelaySeconds;
            }
        }

        protected override void DestroySelf()
        {
            gameObject.SetActive(false);
        }
    }
}
