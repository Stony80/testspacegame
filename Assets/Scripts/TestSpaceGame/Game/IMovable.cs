using UnityEngine;

namespace TestSpaceGame
{
    public interface IMovable
    {
        float Speed
        {
            get; set;
        }

        Vector3 Direction
        {
            get; set;
        }

        Vector3 Position
        {
            get; set;
        }
    }
}
