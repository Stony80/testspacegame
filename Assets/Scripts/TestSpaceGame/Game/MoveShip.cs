using UnityEngine;

namespace TestSpaceGame
{
    public class MoveShip : Movable
    {
        [SerializeField]
        private Rect rect;

        public override Vector3 Position
        {
            //get => base.Position;
            set
            {
                if (rect.Contains(value))
                    base.Position = value;
            }
        }
    }
}
