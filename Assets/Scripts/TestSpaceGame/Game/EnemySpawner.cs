using UnityEngine;
using Zenject;

namespace TestSpaceGame
{
    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField]
        private float _spawnSizeRage;
        [SerializeField]
        private float _respRangeSeconds = 1;
        [SerializeField]
        [Range(2,6)]
        private float _enemyMaxSpeed;

        [Inject]
        private Enemy.Factory enemyFactory;

        private float _next = 0;


        void Update()
        {
            _next -= Time.deltaTime;
            if (_next <= 0)
            {
                Enemy enemy = enemyFactory.Create(Random.Range(1, 4), 1, Random.Range(1, _enemyMaxSpeed));

                Vector3 pos = transform.position;
                pos.y = Random.Range(-_spawnSizeRage, _spawnSizeRage);
                enemy.transform.position = pos;
                enemy.transform.parent = transform.parent;

                _next = Random.Range(0, _respRangeSeconds);
            }
        }
    }
}
