using WG.Lib.StateMachine;

namespace TestSpaceGame
{
    public class GameState : BaseState<IGameView>
    {
        public GameState(IGameView view) : base(StatesEnum.Intro, view)
        {
        }

        override public void DoWork(FiniteStateMachine<StatesEnum> stateMachine)
        {
            if (_view.GameEnded)
                stateMachine.DoTransition(StatesEnum.GameResult);
        }
    }
}