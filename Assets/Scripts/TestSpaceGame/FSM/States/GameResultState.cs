using WG.Lib.StateMachine;

namespace TestSpaceGame
{
    public class GameResultState : BaseState<IGameResultView>
    {
        public GameResultState(IGameResultView view) : base(StatesEnum.Intro, view)
        {
        }

        override public void DoWork(FiniteStateMachine<StatesEnum> stateMachine)
        {
            if (_view.NeedStartGame)
                stateMachine.DoTransition(StatesEnum.Game);
        }
    }
}