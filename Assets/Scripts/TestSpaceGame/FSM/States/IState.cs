using WG.Lib.StateMachine;

namespace TestSpaceGame
{
    public interface IState : IState<StatesEnum>
    {
    }
}
