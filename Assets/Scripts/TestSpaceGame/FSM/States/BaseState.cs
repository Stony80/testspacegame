using WG.Lib.StateMachine;

namespace TestSpaceGame
{
    public abstract class BaseState<TView> : IState
        where TView : IView
    {
        protected TView _view;

        public BaseState(StatesEnum state, TView view)
        {
            Name = state.ToString();
            _view = view;
        }

        public string Name { get; }

        public virtual void OnEnter()
        {
            _view.Show();
        }

        public virtual void OnExit()
        {
            _view.Hide();
        }

        public abstract void DoWork(FiniteStateMachine<StatesEnum> stateMachine);
    }
}
