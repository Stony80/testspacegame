using WG.Lib.StateMachine;

namespace TestSpaceGame
{
    public class IntroState : BaseState<IIntroView>
    {

        public IntroState(IIntroView view) : base(StatesEnum.Intro, view)
        {
        }

        override public void DoWork(FiniteStateMachine<StatesEnum> stateMachine)
        {
            if (_view.NeedStartGame)
                stateMachine.DoTransition(StatesEnum.Game);
        }
    }
}