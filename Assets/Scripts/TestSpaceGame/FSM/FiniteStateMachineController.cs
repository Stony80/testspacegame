﻿using Zenject;

namespace TestSpaceGame
{
    public class FiniteStateMachineController : IInitializable, ITickable
    {
        private FiniteStateMachine _stateMachine;

        [Inject]
        private DiContainer _container;

        public void Initialize()
        {
            IntroState introState = _container.Resolve<IntroState>();
            GameState gameState = _container.Resolve<GameState>();
            GameResultState gameResultState = _container.Resolve<GameResultState>();

            _stateMachine = new FiniteStateMachine(introState);

            _stateMachine.AddTransition(StatesEnum.Game, introState, gameState);
            _stateMachine.AddTransition(StatesEnum.GameResult, gameState, gameResultState);
            _stateMachine.AddTransition(StatesEnum.Game, gameResultState, gameState);
        }

        public void Tick()
        {
            _stateMachine.CurrentState.DoWork(_stateMachine);
        }
    }
}
