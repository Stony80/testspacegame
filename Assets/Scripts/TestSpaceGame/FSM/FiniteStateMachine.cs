using System.Collections.Generic;
using WG.Lib.StateMachine;

namespace TestSpaceGame
{
    public class FiniteStateMachine : FiniteStateMachine<StatesEnum>
    {
        public FiniteStateMachine(IState initialState)
            : base(initialState, new TransitionEventEqualityComparer())
        {
        }
    }

    internal class TransitionEventEqualityComparer : IEqualityComparer<StatesEnum>
    {
        public bool Equals(StatesEnum x, StatesEnum y)
        {
            return x == y;
        }

        public int GetHashCode(StatesEnum obj)
        {
            return (int)obj;
        }
    }
}