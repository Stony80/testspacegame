using UnityEngine;

namespace TestSpaceGame
{
    public class BaseView : MonoBehaviour, IView
    {
        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }
    }
}
