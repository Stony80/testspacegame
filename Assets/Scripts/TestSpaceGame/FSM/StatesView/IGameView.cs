namespace TestSpaceGame
{
    public interface IGameView : IView
    {
        bool GameEnded { get; set; }
    }
}
