namespace TestSpaceGame
{
    public interface IView
    {
        void Show();
        void Hide();
    }
}
