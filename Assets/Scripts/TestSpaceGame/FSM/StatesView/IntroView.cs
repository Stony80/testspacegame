using UnityEngine;
using UnityEngine.UI;

namespace TestSpaceGame
{
    public class IntroView : BaseView, IIntroView
    {
        [SerializeField]
        private Button _startGameBTN;

        public bool NeedStartGame { get; private set; }

        private void Awake()
        {
            _startGameBTN.onClick.AddListener(() => { NeedStartGame = true; });
        }

        private void OnEnable()
        {
            NeedStartGame = false;
        }
    }
}
