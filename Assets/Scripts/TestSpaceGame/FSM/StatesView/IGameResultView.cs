namespace TestSpaceGame
{
    public interface IGameResultView : IView
    {
        bool NeedStartGame { get; }
    }
}
