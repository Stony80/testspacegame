using UnityEngine;

namespace TestSpaceGame
{
    public class GameView : BaseView, IGameView
    {
        [SerializeField]
        private Ship _playerShip;

        public bool GameEnded { get; set; }

        private void OnEnable()
        {
            GameEnded = false;
            _playerShip.Restart();
        }

        private void Update()
        {
            if (_playerShip.Life<=0)
            {
                GameEnded = true;
            }
        }
    }
}
