namespace TestSpaceGame
{
    public interface IIntroView : IView
    {
        bool NeedStartGame { get; }
    }
}
