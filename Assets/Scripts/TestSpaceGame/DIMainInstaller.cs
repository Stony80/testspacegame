using TestSpaceGame;
using UnityEngine;
using Zenject;

public class DIMainInstaller : MonoInstaller
{
    [Header("View")]
    [SerializeField]
    private IntroView _introView;
    [SerializeField]
    private GameView _gameView;
    [SerializeField]
    private GameResultView _gameResultView;
    [Space]

    [Header("Prefabs")]
    [SerializeField]
    private Enemy _enemy;
    [SerializeField]
    private Bullet _bullet;

    public override void InstallBindings()
    {
        // bind views
        Container.BindInstance<IIntroView>(_introView);
        Container.BindInstance<IGameView>(_gameView);
        Container.BindInstance<IGameResultView>(_gameResultView);

        // bind states
        Container.Bind<IntroState>().AsSingle();
        Container.Bind<GameState>().AsSingle();
        Container.Bind<GameResultState>().AsSingle();

        // bind factorys
        Container.BindFactory<int, int, float, Enemy, Enemy.Factory>().FromComponentInNewPrefab(_enemy);
        Container.BindFactory<int, int, float, Bullet, Bullet.Factory>().FromComponentInNewPrefab(_bullet);

        Container.BindInterfacesTo<FiniteStateMachineController>().AsSingle();
    }
}