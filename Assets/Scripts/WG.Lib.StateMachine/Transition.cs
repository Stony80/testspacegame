namespace WG.Lib.StateMachine
{
    public class Transition<T>
    {
        public IState<T> To { get; }

        public IState<T> From { get; }
        
        public Transition(IState<T> from, IState<T> to)
        {
            To = to;
            From = from;
        }

        public override bool Equals(object obj)
        { 
            var other = obj as Transition<T>;
            
            if (other == null)
                return false;
            
            return other.To == To && other.From == From;
        }
    }
}
