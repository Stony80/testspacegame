namespace WG.Lib.StateMachine
{
    /// <summary>
    /// State Machine State Interface
    /// Derive from this to use with FSM
    /// </summary>
    public interface IState<T>
    {
        string Name { get; }

        void OnEnter();
        
        void DoWork(FiniteStateMachine<T> stateMachine);

        void OnExit();
    }
}