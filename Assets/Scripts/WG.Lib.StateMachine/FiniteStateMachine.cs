using System;
using System.Collections.Generic;
using UnityEngine;

namespace WG.Lib.StateMachine
{
	/// <summary>
	/// State Machine
	/// </summary>
	public class FiniteStateMachine<T>
	{
		public IState<T> CurrentState { get; private set; }

		public event Action<Transition<T>> Transitioned;

		private readonly Dictionary<T, List<Transition<T>>> _events;

		public FiniteStateMachine(IState<T> initialState, IEqualityComparer<T> eventIdComparer)
		{
			_events = new Dictionary<T, List<Transition<T>>>(10, eventIdComparer);
			CurrentState = initialState;
			CurrentState.OnEnter();
		}

		/// <summary>
		/// Specifies the event which will trigger the transition from <see cref="from"/> state to <see cref="to"/> state.
		/// The transition will be triggered only if current state is equal to <see cref="from"/> or if <see cref="from"/> is null. 
		/// </summary>
		public void AddTransition(T eventId, IState<T> from, IState<T> to)
		{
			AddTransition(eventId, new Transition<T>(from, to));
		}

		/// <summary>
		/// Specifies the event which will trigger the transition <see cref="transition"/>.
		/// The transition will be triggered only if current state is equal to transition.From or if transition.From is null. 
		/// </summary>
		public void AddTransition(T eventId, Transition<T> transition)
		{
			if (!_events.TryGetValue(eventId, out var eventTransitions))
			{
				eventTransitions = new List<Transition<T>>(3);
				_events.Add(eventId, eventTransitions);
			}
			else
			{
				foreach (var t in eventTransitions)
				{
					if (t.Equals(transition))
					{
						Debug.Log($"Transition exists for event {eventId} from {transition.From} to {transition.To}");
						return;
					}
				}
			}

			eventTransitions.Add(transition);
		}

		public void DoTransition(T eventId)
		{
			if (!_events.TryGetValue(eventId, out var eventTransitions))
			{
				return;
			}

			foreach (var transition in eventTransitions)
			{
				// if source is null then this event trigger transition from any state
				if (transition.From == CurrentState || transition.From == null)
				{
					DoTransition(transition);
					return;
				}
			}
		}

		private void DoTransition(Transition<T> transition)
		{
			CurrentState.OnExit();

			CurrentState = transition.To;

			CurrentState.OnEnter();

			Transitioned?.Invoke(transition);
		}
	}
}